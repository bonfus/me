---
title: Scientific Software
description: A set of toosl for material science
comments: false
weight: 2
---

# unfold.x

Simple code to unfold band structures of first-principles 
supercell calculations.

https://bitbucket.org/bonfus/unfold-x

# muesr

Calculates dipolar field sums for muon spin rotation and relaxation
spectroscopy.

https://github.com/bonfus/muesr \\
https://github.com/bonfus/muLFC

# DBO

Collects and solved the Schrodinger equation using the double adiabatic
approximation for a muon in a crystal.

https://bitbucket.org/bonfus/dbo
