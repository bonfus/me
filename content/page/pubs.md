---
title: Scientific Publicstions
description: List of all my scientific articles
comments: false
weight: 1
---



* *High-pressure magnetic state of MnP probed by means of muon-spin rotation* \\
  R. Khasanov, A. Amato, P. Bonfà, Z. Guguchia, H. Luetkens, E. Morenzoni, R. De Renzi, and N. D. Zhigadlo \\
  Physical Review B (Rapid Communications), **93** 180509 \\
  [DOI: 10.1103/PhysRevB.93.180509](http://dx.doi.org/10.1103/PhysRevB.93.180509)

- - -
                        
* *Magnetic ground state and spin fluctuations in MnGe chiral magnet as studied by muon spin rotation* \\
  N. Martin, M. Deutsch, F. Bert, D. Andreica, A. Amato, P. Bonfà, R. De Renzi, U. K. Rößler, P. Bonville, L. N. Fomicheva, A. V. Tsvyashchenko, and I. Mirebeau \\
  Physical Review B, 93 174405 \\
  [DOI: 10.1103/PhysRevB.93.174405](http://dx.doi.org/10.1103/PhysRevB.93.174405)

- - -

 * *Toward the computational prediction of muon sites and interaction parameters* \\
   P. Bonfà,  R. De Renzi \\
   Journal of the Physical Society of Japan \\
   [DOI: 10.1021/jp5125876](http://dx.doi.org/10.1021/jp5125876)

- - -

  * *Efficient and Reliable Strategy for Identifying Muon Sites Based on the Double Adiabatic Approximation* \\
    P. Bonfà, F. Sartori,  R. De Renzi \\
    The Journal of Physical Chemistry C \\
    [DOI: 10.1021/jp5125876](http://dx.doi.org/10.1021/jp5125876)

- - -

  * *Pair distribution function analysis of La(Fe<sub>1-x</sub>Ru<sub>x</sub>)AsO compounds* \\
    A. Martinelli, A. Palenzona, C. Ferdeghini, M. Mazzani, P. Bonfà, G. Allodi \\
    Journal of Solid State Chemistry, \\
    [DOI: 10.1016/j.jssc.2014.08.001](http://dx.doi.org/10.1016/j.jssc.2014.08.001)


- - -

  * *Tuning the magnetic and structural phase transitions of PrFeAsO via Fe/Ru spin dilution* \\
    Y. Yiu, P. Bonfà, S. Sanna, R. De Renzi, P. Carretta, M. A. McGuire, A. Huq, and S. E. Nagler \\
    Physical Review B (2014) \\
    [DOI: 10.1103/PhysRevB.90.064515](http://dx.doi.org/10.1103/PhysRevB.90.064515)

- - -

  * *Crossover between magnetism and superconductivity in LaFeAsO with low H-doping level* \\
  G Lamura, T Shiroka, P Bonfà, S Sanna, R De Renzi, F Caglieris, MR Cimberle, S Iimura, H Hosono, M Putti \\
  Journal of Physics: Condensed Matter (2014)\\
  [DOI: 10.1088/0953-8984/26/29/295701](http://iopscience.iop.org/0953-8984/26/29/295701)


- - -

  * *Understanding the μSR spectra of MnSi without magnetic polarons* \\
  A Amato, P Dalmas de Réotier, D Andreica, A Yaouanc, A Suter, G Lapertot, IM Pop, E Morenzoni, P Bonfà, F Bernardini, R De Renzi \\
  Physical Review B (2014) \\
  [DOI: 10.1103/PhysRevB.89.184425](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.89.184425)

- - -

  * *Poisoning effect of Mn in LaFe<sub>1-x</sub>Mn<sub>x</sub>AsO<sub>0.89</sub>F<sub>0.11</sub>: Unveiling a quantum critical point in the phase diagram of iron-based superconductors* \\
  F. Hammerath, P. Bonfà, S. Sanna, G. Prando, R. De Renzi, Y. Kobayashi, M. Sato, P. Carretta \\
  Physical Review B (2014) \\
  [DOI: 10.1103/PhysRevB.89.134503](http://journals.aps.org/prb/abstract/10.1103/PhysRevB.89.134503)
 

- - -


  * *75As NQR signature of the isoelectronic nature of ruthenium for iron substitution in LaFe<sub>1-x</sub>Ru<sub>x</sub>AsO* \\
    M. Mazzani, P. Bonfà, G. Allodi, S. Sanna, A. Martinelli, A. Palenzona, P. Manfrinetti, M. Putti, R. De Renzi} \\
    Physica Status Solidi (b) \\
    [DOI: 10.1002/pssb.201350237](http://onlinelibrary.wiley.com/doi/10.1002/pssb.201350237/full)

- - -

  * *Playing quantum hide-and-seek with the muon: localizing muon stopping sites* \\
    J. S. M\"oller, P. Bonfà, D. Ceresoli, F. Bernardini, S. J. Blundell, T. Lancaster, R. De Renzi, N. Marzari, I. Watanabe, S. Sulaiman, M. I. Mohamed-Ibrahim \\
    Physica Scripta (2013), ([DOI](http://dx.doi.org/10.1088/0031-8949/88/06/068510)


- - -

  * *s-wave pairing in the optimally-doped LaO<sub>0.5</sub>F<sub>0.5</sub>BiS<sub>2</sub> superconductor* \\
    G. Lamura, T. Shiroka, P. Bonfà, S. Sanna, R. De Renzi, C. Baines,  H. Luetkens, 
    J. Kajitani, Y. Mizuguchi, O. Miura, K. Deguchi, S. Demura, Y. Takano, and M. Putti \\
 Physical Review B (R) (2013), [DOI](http://link.aps.org/doi/10.1103/PhysRevB.88.180509)

- - -

  * *Role of in-plane and out-of-plane dilution in CeFeAsO: Charge doping versus disorder* \\
    G. Prando, O. Vakaliuk, S. Sanna, G. Lamura, T. Shiroka, P. Bonfà, P. Carretta, R. De Renzi, H.-H. Klauss, C. G. F. Blum, S. Wurmehl, C. Hess, and B. B\"uchner \\
    Physical Review B , [DOI](http://link.aps.org/doi/10.1103/PhysRevB.87.174519)

- - -

  * *Onset of magnetism in optimally electron-doped LFe<sub>1-x</sub>Ru<sub>x</sub>AsO<sub>1-y</sub>F<sub>y</sub> (L = La, Nd, or Sm) superconductors around x=1/4* \\
  S. Sanna, P. Carretta, R. De Renzi, G. Prando, P. Bonfa, M. Mazzani, G. Lamura, T. Shiroka, Y. Kobayashi, M. Sato, R. De Renzi \\
  Physical Review B (2013)\\
  [DOI](http://link.aps.org/doi/10.1103/PhysRevB.87.134518)

- - -

  * *A magnetic glassy phase in Fe<sub>1+y</sub>Se<sub>x</sub>Te<sub>1-x</sub> single crystals* \\
    G. Lamura, T.  Shiroka, P. Bonfà, S. Sanna, F. Bernardini, R. De Renzi, R.  Viennois, E. Giannini, A. Piriou, N. Emery, M.R. Cimberle, M. Putti \\
    Journal of Physics-Condensed Matter (2013), [DOI](http://dx.doi.org/10.1088/0953-8984/25/15/156004)


- - -

  * *Ab initio strategy for muon site assignment in wide band gap fluorides* \\
    F. Bernardini, P. Bonfà, M. Massidda, R. De Renzi \\
    Physical Review B (2013), [DOI](http://link.aps.org/doi/10.1103/PhysRevB.87.115148)
    
- - -

  * *Common effect of chemical and external pressures on the magnetic properties of RCoPO (R = La, Pr)*
    G. Prando,    P. Bonfà,    G. Profeta,    R. Khasanov,    F. Bernardini,   M. Mazzani,     E.M. Bruning,    A. Pal,         V.P.S. Awana,    H.J. Grafe,    B. Buchner,     R. De Renzi,    P. Carretta,    S. Sanna\\
    Physical Review B  (2013), [DOI](http://link.aps.org/doi/10.1103/PhysRevB.87.064401)
    
- - -

  * *Effect of external pressure on the magnetic properties of LnFeAsO (Ln = La, Ce, Pr, Sm)*
    R. De Renzi, P. Bonfà, G. Prando, P. Carretta, R. Khasanov, A.~Amato, H.~Luetkens, M.~Bendele, F. Bernardini, S. Massidda, A. Palenzona, M. Tropeano and M. Vignolo
    Superconductor Science and Technology (2012), [DOI](http://dx.doi.org/10.1088/0953-2048/25/8/084009)
 
- - -

  * *Magnetic properties of spin diluted iron pnictides from $\mu$SR and NMR in LaFe<sub>1-x</sub>Ru<sub>_x</sub>AsO* \\
    P. Bonfà, P. Carretta,  S. Sanna, G. Lamura, G.~Prando, R.~De Renzi, A.~Palenzona, M.~Tropeano, A. Martinelli and M. Putti
    Physical Review B (2011), [DOI](http://link.aps.org/doi/10.1103/PhysRevB.85.054518)

- - -

  * *Correlated trends in nanoscopically coexisting magnetism and superconductivity in optimally electron-doped SmFe<sub>x</sub>Ru<sub>1-x</sub> AsO<sub>0.85</sub>F<sub>0.15</sub>*
    S. Sanna, P. Carretta, P. Bonfà, G. Prando, G. Allodi, R. De Renzi, T. Shiroka, G. Lamura, A. Martinelli, M. Putti
    Physical Review Letters (2011), [DOI](http://link.aps.org/doi/10.1103/PhysRevLett.107.227003}{10.1103/PhysRevLett.107.227003) 
